# Wordpress on Kubenetes with Let's Encrypt
-------------------------------------------

1. Export your KUBECONFIG environment variable

        $ export KUBECONFIG=/path/to/kube/conf

2. Install the Helm client (https://helm.sh/) on your local machine

3. Create a Tiller cluster user with the appropriate permission(s) for installing and managing charts

        $ kubectl create -f requirements/helm.rbac.yml

4. Install Tiller on your cluster and use the service account created previously

        $ helm init --service-account tiller

5. Confirm that the Tiller daemon is installed on your cluster

        $ kubectl get pods --namespace kube-system
        $ kubectl get services --namespace kube-system
        $ helm version

6. Create the MySQL root user's password as a Kubernetes secret, apply the deployment to the cluster and test the access to the MySQL server

        $ cd manifests/
        $ kubectl create secret generic mysql-pass --from-literal=password=MYSQL_ROOT_PASSWORD
        $ kubectl apply -f mysql.deployment.yml
        $ kubectl get pods
        $ kubectl exec -it MYSQL_POD_NAME bash
        $ mysql -uroot -p

7. Apply the Wordpress deployment to the cluster

        $ kubectl apply -f wordpress.deployment.yml

8. Install the nginx Ingress Controller using Helm

        $ helm install stable/nginx-ingress
        $ kubectl get services

9. List the cluster's services, and make your domain name point to the nginx Ingress Controller external IP

        $ kubectl get services

9. Edit the nginx Ingress Controller manifest and change the DOMAIN_NAME placeholder value with the domain name you want to use and apply the ingress manifest

        $ kubectl apply -f nginx.ingress.yml

10. Access your Wordpress installation by opening a browser at the https://DOMAIN_NAME and accept the invalid certificate warning

11. Install the cert-manager with its value manifest using Helm

        $ helm install -f cert-manager.values.yml stable/cert-manager
        $ kubectl get pods

12. Edit the Let's Encrypt manifest EMAIL_ADDRESS placeholder value and apply the manifest

        $ kubectl create -f letsencrypt.clusterissuer.yml
        $ kubectl get clusterissuer

13. List the cluster's pods, check the cert-manager pod logs and make sure the SSL certificate for your domain has been generated

        $ kubectl get pods
        $ kubectl logs CERT_MANAGER_POD_NAME
        ...
        successfully obtained certificate: cn="DOMAIN_NAME" altNames=[DOMAIN_NAME] url="https://acme-v02.api.letsencrypt.org/acme/order/xxxx/yyyy"
        ...

14. Access your Wordpress installation by opening a browser at the https://DOMAIN_NAME and confirm that the certificate is valid
